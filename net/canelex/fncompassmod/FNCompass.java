package net.canelex.fncompassmod;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraftforge.common.config.Configuration;
import org.lwjgl.opengl.GL11;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import java.awt.Color;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.Minecraft;

public class FNCompass
{
    private Minecraft mc;
    private FontRenderer fr;
    private final double PRIMARY_CARDINAL_SCALE = 1.5;
    private final double SECONDARY_CARDINAL_SCALE = 1.0;
    private final double ANGLE_SCALE = 0.75;
    public boolean enabled;
    public int details;
    public int offX;
    public int offY;
    public int width;
    public int height;
    public int cwidth;
    public boolean background;
    public boolean chroma;
    public boolean shadow;
    public int tintMarker;
    public int tintDirection;
    private int offsetAll;
    private int centerX;
    private int colorMarker;
    private int colorDirection;
    
    public FNCompass(final Minecraft mc) {
        this.mc = mc;
        this.fr = mc.fontRenderer;
    }
    
    public void drawCompass(final int screenWidth) {
        final int direction = this.normalize((int)this.mc.player.rotationYaw);
        this.offsetAll = this.cwidth * direction / 360;
        this.centerX = screenWidth / 2 + this.offX;
        if (this.background) {
            Gui.drawRect(this.centerX - this.width / 2, this.offY, this.centerX + this.width / 2, this.offY + this.height, 1140850688 );
        }
        if (!this.chroma) {
            if (this.tintMarker != 0) {
                this.colorMarker = Color.HSBtoRGB((float)this.tintMarker / 100.0f, 1.0f, 1.0f);
            }
            else {
                this.colorMarker = -1;
            }
            if (this.tintDirection != 0) {
                this.colorDirection = Color.HSBtoRGB((float)this.tintDirection / 100.0f, 1.0f, 1.0f);
            }
            else {
                this.colorDirection = -1;
            }
        }
        else {
            final int hsBtoRGB = Color.HSBtoRGB((float)(System.currentTimeMillis() % 3000L) / 3000.0f, 1.0f, 1.0f);
            this.colorMarker = hsBtoRGB;
            this.colorDirection = hsBtoRGB;
        }
        this.renderMarker();
        if (this.details >= 0) {
            this.drawDirection("S", 0, 1.25);
            this.drawDirection("W", 90, 1.25);
            this.drawDirection("N", 180, 1.25);
            this.drawDirection("E", 270, 1.25);
        }
        if (this.details >= 1) {
            this.drawDirection("SW", 45, 1.25);
            this.drawDirection("NW", 135, 1.25);
            this.drawDirection("NE", 225, 1.25);
            this.drawDirection("SE", 315, 1.25);
        }
        if (this.details >= 2) {
            for(float i = 0; i < 360; i+=5)
            {
                if(i % 45 != 0 && i % 5 == 0)
                {
                    this.drawDirection(Integer.toString((int)i), (int)i, .75);
                }
            }
//            this.drawDirection("15", 15, 0.75);
//            this.drawDirection("30", 30, 0.75);
//            this.drawDirection("45", 45, 0.75);
//            this.drawDirection("60", 60, 0.75);
//            this.drawDirection("75", 75, 0.75);
//
//            this.drawDirection("105", 105, 0.75);
//            this.drawDirection("120", 120, 0.75);
//            this.drawDirection("135", 135, 0.75);
//            this.drawDirection("150", 150, 0.75);
//            this.drawDirection("165", 165, 0.75);
//
//            this.drawDirection("195", 195, 0.75);
//            this.drawDirection("210", 210, 0.75);
//            this.drawDirection("225", 225, 0.75);
//            this.drawDirection("240", 240, 0.75);
//            this.drawDirection("255", 255, 0.75);
//
//            this.drawDirection("285", 285, 0.75);
//            this.drawDirection("300", 300, 0.75);
//            this.drawDirection("315", 315, 0.75);
//            this.drawDirection("330", 330, 0.75);
//            this.drawDirection("345", 345, 0.75);
        }
    }
    
    private void renderMarker() {
        final Tessellator tessellator = Tessellator.getInstance();
        final BufferBuilder buf = tessellator.getBuffer();
        GlStateManager.enableBlend();
        GlStateManager.disableTexture2D();
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
        GlStateManager.color((float)(this.colorMarker >> 16 & 0xFF) / 255.0f, (float)(this.colorMarker >> 8 & 0xFF) / 255.0f, (float)(this.colorMarker & 0xFF) / 255.0f, 1.0f);
        buf.begin(6, DefaultVertexFormats.POSITION);
        buf.pos((double)this.centerX, (double)(this.offY + 3), 0.0).endVertex();
        buf.pos((double)(this.centerX + 3), (double)this.offY, 0.0).endVertex();
        buf.pos((double)(this.centerX - 3), (double)this.offY, 0.0).endVertex();
        tessellator.draw();
        GlStateManager.enableTexture2D();
        GlStateManager.disableBlend();
    }
    
    private void drawDirection(final String dir, final int degrees, final double scale) {
        int offset = this.cwidth * degrees / 360 - this.offsetAll;
        if (offset > this.cwidth / 2) {
            offset -= this.cwidth;
        }
        if (offset < -this.cwidth / 2) {
            offset += this.cwidth;
        }
        final double opacity = 1.0 - Math.abs(offset) / (this.width / 2.0);
        if (opacity > 0.1) {
            final int defcolor = this.colorDirection & 0xFFFFFF;
            final int color = defcolor | (int)(opacity * 255.0) << 24;
            final int posX = this.centerX + offset - (int)(this.fr.getStringWidth(dir) * scale / 2.0);
            final int posY = this.offY + this.height / 2 - (int)(this.fr.FONT_HEIGHT * scale / 2.0);
            GL11.glEnable(3042);
            GL11.glPushMatrix();
            GL11.glTranslated((double)(-posX) * (scale - 1.0), (double)(-posY) * (scale - 1.0), 0.0);
            GL11.glScaled(scale, scale, 1.0);
            if (this.shadow) {
                this.fr.drawStringWithShadow(dir, (float)posX, (float)posY, color);
            }
            else {
                this.fr.drawString(dir, posX, posY, color);
            }
            GL11.glPopMatrix();
            GL11.glDisable(3042);
        }
    }
    
    public void save(final Configuration config) {
        config.get("general", "enabled", true).set(this.enabled);
        config.get("general", "details", 2).set(this.details);
        config.get("position", "offX", 0).set(this.offX);
        config.get("position", "offY", 0).set(this.offY);
        config.get("scale", "width", 150).set(this.width);
        config.get("scale", "height", 20).set(this.height);
        config.get("scale", "cwidth", 500).set(this.cwidth);
        config.get("color", "background", true).set(this.background);
        config.get("color", "chroma", false).set(this.chroma);
        config.get("color", "shadow", true).set(this.shadow);
        config.get("color", "tintMarker", 0).set(this.tintMarker);
        config.get("color", "tintDirection", 0).set(this.tintDirection);
    }
    
    public void load(final Configuration config) {
        this.enabled = config.get("general", "enabled", true).getBoolean();
        this.details = config.get("general", "details", 2).getInt();
        this.offX = config.get("position", "offX", 0).getInt();
        this.offY = config.get("position", "offY", 0).getInt();
        this.width = config.get("scale", "width", 150).getInt();
        this.height = config.get("scale", "height", 20).getInt();
        this.cwidth = config.get("scale", "cwidth", 500).getInt();
        this.background = config.get("color", "background", true).getBoolean();
        this.chroma = config.get("color", "chroma", false).getBoolean();
        this.shadow = config.get("color", "shadow", true).getBoolean();
        this.tintMarker = config.get("color", "tintMarker", 0).getInt();
        this.tintDirection = config.get("color", "tintDirection", 0).getInt();
    }
    
    private int normalize(int direction) {
        if (direction > 360) {
            direction %= 360;
        }
        while (direction < 0) {
            direction += 360;
        }
        return direction;
    }
}
