package net.canelex.fncompassmod;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.gui.GuiChat;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraft.command.ICommand;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.common.config.Configuration;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.Mod;

@Mod(modid = "fncompassmod", version = "1.0")
public class CompassMod
{
    private Minecraft mc;
    private FNCompass compass;
    private Configuration config;
    
    @Mod.EventHandler
    public void preinit(final FMLPreInitializationEvent event) {
        this.config = new Configuration(event.getSuggestedConfigurationFile());
    }
    
    @Mod.EventHandler
    public void init(final FMLInitializationEvent event) {
        this.mc = Minecraft.getMinecraft();
        this.compass = new FNCompass(this.mc);
        this.loadConfig();
        MinecraftForge.EVENT_BUS.register((Object)this);
        ClientCommandHandler.instance.registerCommand((ICommand)new CommandEditCompass(this));
    }
    
    @SubscribeEvent
    public void onRenderOverlay(final RenderGameOverlayEvent.Post event) {
        if (event.getType() == RenderGameOverlayEvent.ElementType.ALL) {
            if (!this.compass.enabled) {
                return;
            }
            if (this.mc.player == null) {
                return;
            }
            if (this.mc.currentScreen != null && !(this.mc.currentScreen instanceof GuiChat)) {
                return;
            }
            this.compass.drawCompass(event.getResolution().getScaledWidth());
        }
    }
    
    public void saveConfig() {
        this.compass.save(this.config);
        this.config.save();
    }
    
    public void loadConfig() {
        this.config.load();
        this.compass.load(this.config);
    }
    
    public FNCompass getCompass() {
        return this.compass;
    }
}
