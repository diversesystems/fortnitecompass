package net.canelex.fncompassmod.gui;

import java.io.IOException;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.canelex.fncompassmod.CompassMod;
import net.minecraftforge.fml.client.config.GuiSlider;

public class GuiCompassStyle extends GuiCompass
{
    private GuiSlider sliderMarkerTint;
    private GuiSlider sliderDirectionTint;
    
    public GuiCompassStyle(final CompassMod mod, final GuiScreen parent) {
        super(mod, parent);
    }
    
    public void initGui() {
        this.buttonList.add(new GuiButton(0, this.width / 2 - 60, this.height / 2 - 75, 120, 20, this.getColoredBool("Background: ", this.compass.background)));
        this.buttonList.add(new GuiButton(1, this.width / 2 - 60, this.height / 2 - 50, 120, 20, this.getColoredBool("Chroma: ", this.compass.chroma)));
        this.buttonList.add(new GuiButton(2, this.width / 2 - 60, this.height / 2 - 25, 120, 20, this.getColoredBool("Shadow: ", this.compass.shadow)));
        this.buttonList.add(this.sliderMarkerTint = new GuiSlider(3, this.width / 2 - 60, this.height / 2, 120, 20, "Marker Tint: ", "", 0.0, 100.0, (double)this.compass.tintMarker, false, true));
        this.buttonList.add(this.sliderDirectionTint = new GuiSlider(4, this.width / 2 - 60, this.height / 2 + 25, 120, 20, "Direction Tint: ", "", 0.0, 100.0, (double)this.compass.tintDirection, false, true));
        this.buttonList.add(new GuiButton(5, this.width / 2 - 60, this.height / 2 + 50, 120, 20, "Done"));
    }
    
    protected void actionPerformed(final GuiButton button) throws IOException {
        switch (button.id) {
            case 0: {
                this.compass.background = !this.compass.background;
                button.displayString = this.getColoredBool("Background: ", this.compass.background);
                break;
            }
            case 1: {
                this.compass.chroma = !this.compass.chroma;
                button.displayString = this.getColoredBool("Chroma: ", this.compass.chroma);
                break;
            }
            case 2: {
                this.compass.shadow = !this.compass.shadow;
                button.displayString = this.getColoredBool("Shadow: ", this.compass.shadow);
                break;
            }
            case 5: {
                this.mc.displayGuiScreen(this.parent);
                break;
            }
        }
    }
    
    @Override
    protected void mouseClicked(final int mouseX, final int mouseY, final int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        this.updateSliders();
    }
    
    protected void mouseClickMove(final int mouseX, final int mouseY, final int clickedMouseButton, final long timeSinceLastClick) {
        super.mouseClickMove(mouseX, mouseY, clickedMouseButton, timeSinceLastClick);
        this.updateSliders();
    }
    
    @Override
    protected void mouseReleased(final int mouseX, final int mouseY, final int state) {
        super.mouseReleased(mouseX, mouseY, state);
        this.updateSliders();
    }
    
    private void updateSliders() {
        this.compass.tintMarker = this.sliderMarkerTint.getValueInt();
        this.compass.tintDirection = this.sliderDirectionTint.getValueInt();
    }
}
