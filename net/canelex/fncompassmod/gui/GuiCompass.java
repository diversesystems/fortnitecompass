package net.canelex.fncompassmod.gui;

import net.minecraft.util.text.TextFormatting;
import java.io.IOException;
import net.canelex.fncompassmod.FNCompass;
import net.canelex.fncompassmod.CompassMod;
import net.minecraft.client.gui.GuiScreen;

public class GuiCompass extends GuiScreen
{
    protected CompassMod mod;
    protected FNCompass compass;
    protected GuiScreen parent;
    private boolean dragging;
    private int lastX;
    private int lastY;
    
    public GuiCompass(final CompassMod mod, final GuiScreen parent) {
        this.mod = mod;
        this.compass = mod.getCompass();
        this.parent = parent;
    }
    
    public void drawScreen(final int mouseX, final int mouseY, final float partialTicks) {
        this.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, partialTicks);
        if (this.compass.enabled) {
            this.compass.drawCompass(this.width);
        }
        if (this.dragging) {
            final FNCompass compass = this.compass;
            compass.offX += mouseX - this.lastX;
            final FNCompass compass2 = this.compass;
            compass2.offY += mouseY - this.lastY;
        }
        this.lastX = mouseX;
        this.lastY = mouseY;
    }
    
    protected void mouseClicked(final int mouseX, final int mouseY, final int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        if (mouseX >= (this.width - this.compass.width) / 2 + this.compass.offX && mouseY >= this.compass.offY && mouseX <= (this.width + this.compass.width) / 2 + this.compass.offX && mouseY <= this.compass.offY + this.compass.height) {
            this.dragging = true;
            this.lastX = mouseX;
            this.lastY = mouseY;
        }
    }
    
    protected void mouseReleased(final int mouseX, final int mouseY, final int state) {
        super.mouseReleased(mouseX, mouseY, state);
        this.dragging = false;
    }
    
    public void onGuiClosed() {
        this.mod.saveConfig();
    }
    
    protected String getColoredBool(final String prefix, final boolean bool) {
        if (bool) {
            return prefix + TextFormatting.GREEN + "TRUE";
        }
        return prefix + TextFormatting.RED + "FALSE";
    }
}
