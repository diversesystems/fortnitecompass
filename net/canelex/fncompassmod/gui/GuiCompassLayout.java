package net.canelex.fncompassmod.gui;

import net.minecraft.util.text.TextFormatting;
import java.io.IOException;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.canelex.fncompassmod.CompassMod;
import net.minecraftforge.fml.client.config.GuiSlider;

public class GuiCompassLayout extends GuiCompass
{
    private GuiSlider sliderWidth;
    private GuiSlider sliderCWidth;
    
    public GuiCompassLayout(final CompassMod mod, final GuiScreen parent) {
        super(mod, parent);
    }
    
    public void initGui() {
        this.buttonList.add(new GuiButton(0, this.width / 2 - 60, this.height / 2 - 25, 120, 20, this.getDetailString()));
        this.buttonList.add(this.sliderWidth = new GuiSlider(1, this.width / 2 - 60, this.height / 2, 120, 20, "Width: ", "", 50.0, 720.0, (double)this.compass.width, false, true));
        this.buttonList.add(this.sliderCWidth = new GuiSlider(2, this.width / 2 - 60, this.height / 2 + 25, 120, 20, "Spacing: ", "", 200.0, 1920.0, (double)this.compass.cwidth, false, true));
        this.buttonList.add(new GuiButton(3, this.width / 2 - 60, this.height / 2 + 50, 120, 20, "Done"));
    }
    
    protected void actionPerformed(final GuiButton button) throws IOException {
        switch (button.id) {
            case 0: {
                this.compass.details = (this.compass.details + 1) % 3;
                button.displayString = this.getDetailString();
                break;
            }
            case 3: {
                this.mc.displayGuiScreen(this.parent);
                break;
            }
        }
    }
    
    @Override
    protected void mouseClicked(final int mouseX, final int mouseY, final int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        this.updateSliders();
    }
    
    protected void mouseClickMove(final int mouseX, final int mouseY, final int clickedMouseButton, final long timeSinceLastClick) {
        super.mouseClickMove(mouseX, mouseY, clickedMouseButton, timeSinceLastClick);
        this.updateSliders();
    }
    
    @Override
    protected void mouseReleased(final int mouseX, final int mouseY, final int state) {
        super.mouseReleased(mouseX, mouseY, state);
        this.updateSliders();
    }
    
    private void updateSliders() {
        this.compass.width = this.sliderWidth.getValueInt();
        this.compass.cwidth = this.sliderCWidth.getValueInt();
    }
    
    private String getDetailString() {
        switch (this.compass.details) {
            case 0: {
                return "Details: " + TextFormatting.GOLD + "LOW";
            }
            case 1: {
                return "Details: " + TextFormatting.YELLOW + "MED";
            }
            case 2: {
                return "Details: " + TextFormatting.GREEN + "HIGH";
            }
            default: {
                return "Details: ???";
            }
        }
    }
}
