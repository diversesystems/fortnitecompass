package net.canelex.fncompassmod;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.gui.GuiScreen;
import net.canelex.fncompassmod.gui.GuiCompassHub;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraft.command.CommandException;
import net.minecraftforge.common.MinecraftForge;
import net.minecraft.server.MinecraftServer;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.CommandBase;

public class CommandEditCompass extends CommandBase
{
    private CompassMod mod;
    
    public CommandEditCompass(final CompassMod mod) {
        this.mod = mod;
    }
    
    public String getName() {
        return "compassmod";
    }
    
    public String getUsage(final ICommandSender sender) {
        return "/compassmod";
    }
    
    public boolean checkPermission(final MinecraftServer server, final ICommandSender sender) {
        return true;
    }
    
    public void execute(final MinecraftServer server, final ICommandSender sender, final String[] args) throws CommandException {
        MinecraftForge.EVENT_BUS.register((Object)this);
    }
    
    @SubscribeEvent
    public void onClientTick(final TickEvent.ClientTickEvent event) {
        MinecraftForge.EVENT_BUS.unregister((Object)this);
        Minecraft.getMinecraft().displayGuiScreen((GuiScreen)new GuiCompassHub(this.mod));
    }
}
